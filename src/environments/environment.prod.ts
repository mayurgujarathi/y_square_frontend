export const environment = {
  production: true,
  baseUrl: 'http://localhost:8080/api/v1',
  socketUrl: 'http://127.0.0.1:8080'
};
