import { Injectable } from '@angular/core';
import { io, Socket } from 'socket.io-client';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  private socketUrl: any;
  private user: any;

  constructor(private cookieService: CookieService) { }

  // connect() {
  //   this.user = JSON.parse(this.cookieService.get('user'));
    
  //   this.socketUrl = io("http://localhost:8080", {
  //     query: { user_id: this.user.id }, 
  //     reconnectionDelay: 1000,
  //     reconnection: true,
  //     reconnectionAttempts: 10,
  //     transports: ['websocket'],
  //     agent: false, 
  //     upgrade: false,
  //     rejectUnauthorized: false
  //   });
  //   console.log('connecting to socket.......................', this.socketUrl);
  
  //   let observable = new Observable(observer => {
  //     this.socketUrl.on('active_users', (data: any) => {
  //       console.log('Received message from Websocket Server', data);
  //       observer.next(data);
  //     });

  //     return () => {
  //       this.socketUrl.disconnect();
  //     }
  //   });

  // }
}
