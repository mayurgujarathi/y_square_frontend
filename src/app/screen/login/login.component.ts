import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../login/user';
import { LoginService } from './login.service';
import { CookieService } from 'ngx-cookie-service';
import { SocketService } from './socket.service'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user = new User('', '', '', '');
  isRegister: boolean = false;
  isLogin: boolean = true;
  errorMessage: any;

  constructor(private router: Router,
    private loginService: LoginService,
    private cookieService: CookieService,
    private socketService: SocketService) {
  }

  ngOnInit(): void {
    console.log('ngOnInit called');
  }

  ngOnDestroy() {
    console.log('ngOnDestroy called');
  }

  login() {
    this.loginService.login(this.user).subscribe((response: any) => {
      console.log('login response received: ', response);
      let resp = response;
      if (resp.status == 200) {
        this.cookieService.set('user', JSON.stringify(resp.data));
        // this.socketService.connect();
        this.router.navigate(['/dashboard']);
      }
    },
      (error: any) => {
        console.log(error);
        this.errorMessage = error;
      });
  }

  register() {
    console.log('in register page', this.isRegister)
  }
}
