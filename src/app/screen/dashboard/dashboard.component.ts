import { Component, OnInit } from '@angular/core';
import { DashboardService } from './dashboard.service';
import { Observable } from 'rxjs';
import { SocketService } from '../login/socket.service'
import { CookieService } from 'ngx-cookie-service';
import { io, Socket } from 'socket.io-client';
import { MessageService } from 'primeng/api';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [MessageService]
})
export class DashboardComponent implements OnInit {
  fileToUpload: any;
  users: any;
  verfication_code: any;
  user: any;
  socket: any;
  currentUser: any;
  onlineUsers: any;

  constructor(private dashboardService: DashboardService,
              private cookieService: CookieService,
              private messageService: MessageService) {
    this.user = JSON.parse(this.cookieService.get('user'));
    this.currentUser = this.user;
                console.log('this.currentUser', JSON.stringify(this.currentUser));
    this.socket = io(environment.socketUrl, {
      query: { user_id: this.user.id },
      reconnectionDelay: 1000,
      reconnection: true,
      reconnectionAttempts: 10,
      transports: ['websocket'],
      agent: false,
      upgrade: false,
      rejectUnauthorized: false
    });
  }

  ngOnInit(): void {
    this.getAllUsers();
    this.socket.on('active_users', (data: any) => {
      this.users = data;
    });

    this.socket.on('receive_notification', (data: any) => {
      console.log('Notification received...');
      this.messageService.add({ key: 'tl', severity: 'info', summary: 'Info', detail: 'Message Content' });
    });

    this.socket.on('refresh_user_list', (data: any) => {
      this.onlineUsers = data.data;
      console.log('THIS LIVE USER LIST: ', this.onlineUsers);

    });
  }

  getAllUsers() {
    this.dashboardService.getAllUsers().subscribe((response: any) => {
      this.users = response;
      console.log('users received: ', this.users)
    });
  }

  openUser(socketId: string) {
    console.log('opning users', socketId);
    this.socket.emit('send_notification', { socketId: socketId });
  }
}
