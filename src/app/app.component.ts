import { Component } from '@angular/core';
import { SocketService } from '../app/screen/login/socket.service';
import { io, Socket } from 'socket.io-client';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'mobigic';
  private socket: any;
  private user: any;

  constructor (private socketService: SocketService, private cookieService: CookieService) {
    
   }

  ngOnInit() {
    
  }
 }
